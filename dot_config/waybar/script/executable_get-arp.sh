#!/bin/bash

# Fonction pour obtenir les informations ARP
get_arp_info() {
    arp_output=$(ip neigh)

    # Compte le nombre de lignes dans la sortie ARP
    num_lines=$(echo "$arp_output" | wc -l)

    if [ "$num_lines" -gt 1 ]; then
        echo "$arp_output" | awk '{print $1 " -> " $5}' | tail -n +2 | head -n 1
    else
        echo "No ARP entries"
    fi
}

# Appel de la fonction pour obtenir les informations ARP
get_arp_info
