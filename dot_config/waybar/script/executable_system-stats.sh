#!/bin/bash

# Get CPU usage
cpu=$(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1}')

# Get GPU usage (assuming nvidia-smi is installed)
gpu=$(nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader,nounits | awk '{print $1}')

# Get RAM usage
ram=$(free | grep Mem | awk '{print $3/$2 * 100.0}')

# Get Disk usage (assuming root filesystem)
disk=$(df / | tail -1 | awk '{print $5}' | sed 's/%//')

# Output JSON for waybar
echo -e "{\"cpu\":\"$cpu\",\"gpu\":\"$gpu\",\"ram\":\"$ram\",\"disk\":\"$disk\",\"icon\":\"\"}"
